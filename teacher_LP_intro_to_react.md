### Intro To React: ###

# Objectives: #

    1. Students will be able to Create and Use React Components
    2. Students will be able to pass props to components

# Glossary: #

    Component : Reusable independent bits of code
    Props: Also known as a property. Global variable passed into components as arguments. 

# Introduction: #
>React is an open-source Javascript library for building fast and interactive user interfaces for the web as well as mobile applications. React allows you to create re-usable and reactive components consisting of HTML, JavaScript, and CSS. It was developed by Facebook in 2011 and is the most popular javascript library for building user interfaces

# Component: # 
>React is all about components. Components are essentially a piece of the user interface. When building with react we use a bunch of independent isolated and reusable components to combine them to create or build complex user interfaces. Think of a website using React as legos. You have individual legos (individual components) and when you combine those individual legos you can build and create many different lego structures (websites). You can create multiple small components that will work together to create one complete website.


NOTE: Due to the environment that our learners currently are in we will not be using “create-react app”


# File Structure: #
	
>We will want to start off by setting up our file structure. Starting off with the root folder being the name of our React app “my_react_app”. Next we will want a public and src folder. The public folder will host all of our front end code and the src will host all of our back end code. Inside the public folder we want to have index.html and in our src folder we want to have index.js

        my_react_app
            public
                Index.html
            src
                index.js

# Setting Up Front End (/public/index.html): #

>In our index.html all we want to use here is the boiler template that VS Code can already set up for us using `shift + !` . Within our `<body>` tag we want to create a `<div>` with an id of root

```html
    <body>
    <div id="root">
                
    </div>
    </body>
```

# Setting Up (/src/index.js): #
When we set up our index.js file within our src folder, after creating this file we want to make sure that we include the following code:

```jsx
    import React from "react";
    import { createRoot } from 'react-dom/client';
        
    function App(){
    return(
        <div>
            <h2>Welcome To React</h2>
        </div>
    );
    };

    createRoot(document.querySelector("#root")).render(<App/>);
```

# Installing Node Modules: #

Using the terminal to ensure that we are in the root of the “my_react_app”. The we want to run:

1.`npm init -y`
    
    This will set up our package.json file with default values (That's what the “-y” is for)

2.`npm i react react-dom react-scripts`

    This will install all the necessary node modules to run react
        React
        React-Dom
        React-Scripts
Make sure that the scripts property in package.json is equal to:

```json
    "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test"
    }
```

# Creating Our Own Custom Component: #

You can create components all on one file. But it is considered best practice to create new components in new files. So that you have one file per component. That way it keeps your code nice and clean. Within the /src folder create another folder called component which will hold all of our components :

    src
        index.js
        components

Here, index.js is technically a file that renders your component file so we will keep it outside of the component folder because it is our root component. index.js will hold all of our other components or they will be nested within.

Looking back at our src/index.js” you can see we already have a component here:

```jsx
    function App(){
    return(
        <div>
            <h2>Welcome To React</h2>
        </div>
    );
    };
```

A component is simply a function. But this function as you can tell is a little different. We declare our function and what we put in our return should be whatever you would want to be displayed on the website.

Lets add a component called “MyName” that is going to display “My Name Is :” onto our html page:

1. Create MyName.js in components folder
    src
        index.js
        components
            MyName.js

2. In this folder (src/components/MyName.js), we want to create a function called ”MyName” and all we want to have this function do is return a HTML `<h3>` tag with the text “My Name is:” This is going to allow us to set up the page to have content be dynamically loaded onto it:

```jsx
    function MyName(){
    return  <h3>My Name Is:</h3>
    };
```

3. Next we are going to want to export the function. By exporting the function this will allow us to be able to access it within other JavaScript files. We will do this by using the “export default”  module that is included within the ES6 standardization of JavaScript :

```jsx
    function MyName(){
    return  <h3>My Name Is:</h3>
    };

    export default MyName;
```

4. Now that we have our function exported, we need to import it into our src/index.js. To do that we want to use the “import” module that is included within ES6 standardization. The import statement is going to include a name that will define whatever is being imported and the path that leads to the file that we are trying to reference.

```js
    import React from "react";
    import { createRoot } from 'react-dom/client';
    import MyName from "./components/MyName";

```

5. In the last step we were able to import our component into our src/index.js file. Now that we have that imported we can use that component with the name that we gave it. 

```jsx
    function App(){
    return(
        <div>
            <h2>Welcome To React</h2>
            <MyName></MyName>
        </div>
    );
    };
```

The syntax for adding this component looks relatively similiar to HTML syntax. This is because behind the scenes React uses another markip language called JSX which stands for JavaScriptXML. We will not cover JSX in this lesson because React uses it behind the scenes. We will however look at how to actually pass information to our component next.

# Using Props: #

Now That we understand how to create components, let's take a look at how to pass data into our component.

1. We want to add our data to our root component or root function. Remember data can only be passed down to components. We will add two constant variables called 
FirstName and LastName. Which will equal your first name and your last name.

```jsx
    function App(){
    const FirstName = "Jonathan";
    const LastName = "Vershum";
    
    return(
        <div>
            <h2>Welcome To React</h2>
            <MyName></MyName>
        </div>
    );
    };
```

2. Next inside of our App() component We currently have the <MyName> Component, within the <MyName> Component we will add MyFirstName={FirstName} and MyLastName={LastName}.

    <MyName MyFirstName={FirstName} MyLastName={LastName}></MyName>

>What we did here was define 2 objects that are arguments for our MyName Function 

3. Next we will access those variables inside of our MyName component using “props”. Navigate to components/MyName.js and inside of the MyName function parameter we will add props

```jsx
    function MyName(props){
    return  <h3>My Name Is:</h3>
    };
```

By adding props this will allow us to access the global props object, which is where our MyFirstName and MyLastName are currently being stored.

4. Now all we have to do is access the data within the props object. Before our closing ```</h3>``` tag is where we want our First and Last Name to appear. Here we will add ```{props.MyFirstName}``` and ```{props.MyLastName}``` which is how javascript and react is going to access the data that we passed to this function using the props object

```jsx
    function MyName(props){
    return  <h3>My Name Is: {props.MyFirstName} {props.MyLastName}</h3>
    };
        
    export default MyName;
```


This is how props can pass information to our components to help render information on the website. Now when React renders the page your first name and last name should be displayed on the website.
