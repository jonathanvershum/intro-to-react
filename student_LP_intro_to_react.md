### Intro To React: ###

# Objectives: #

    1. Students will be able to Create and Use React Components
    2. Students will be able to pass props to components

# Glossary: #

    Component : Reusable independent bits of code
    Props: Also known as a property. Global variable passed into components as arguments.

#File Structure: #

>We will want to start off by setting up our file structure. Starting off with the root folder being the name of our React app “my_react_app”. Next we will want a public and src folder. The public folder will host all of our front end code and the src will host all of our back end code. Inside the public folder we want to have index.html and in our src folder we want to have index.js

        my_react_app
            public
                Index.html
            src
                index.js

# Setting Up (/src/index.js): #
When we set up our index.js file within our src folder, after creating this file we want to make sure that we include the following code:

```jsx
    import React from "react";
    import { createRoot } from 'react-dom/client';
        
    function App(){
    return(
        <div>
            <h2>Welcome To React</h2>
        </div>
    );
    };

    createRoot(document.querySelector("#root")).render(<App/>);
```

# Installing Node Modules: #

Using the terminal to ensure that we are in the root of the “my_react_app”. The we want to run:

1.`npm init -y`
    
    This will set up our package.json file with default values (That's what the “-y” is for)

2.`npm i react react-dom react-scripts`

    This will install all the necessary node modules to run react
        React
        React-Dom
        React-Scripts

Make sure that the scripts property in package.json is equal to:

```json
    "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test"
    }
```
#Creating Our Own Custom Component: #

Lets add a component called “MyName” that is going to display “My Name Is :” onto our html page:

1. Create MyName.js in components folder
    src
        index.js
        components
            MyName.js

2. In this folder (src/components/MyName.js), we want to create a function called ”MyName” and all we want to have this function do is return a HTML `<h3>` tag with the text “My Name is:” This is going to allow us to set up the page to have content be dynamically loaded onto it:

```jsx
    function MyName(){
    return  <h3>My Name Is:</h3>
    };
```

3. Next we are going to want to export the function. By exporting the function this will allow us to be able to access it within other JavaScript files. We will do this by using the “export default”  module that is included within the ES6 standardization of JavaScript :

```jsx
    function MyName(){
    return  <h3>My Name Is:</h3>
    };

    export default MyName;
```

4. Now that we have our function exported, we need to import it into our src/index.js. To do that we want to use the “import” module that is included within ES6 standardization. The import statement is going to include a name that will define whatever is being imported and the path that leads to the file that we are trying to reference.

```js
    import React from "react";
    import { createRoot } from 'react-dom/client';
    import MyName from "./components/MyName";

```

5. In the last step we were able to import our component into our src/index.js file. Now that we have that imported we can use that component with the name that we gave it. 

```jsx
    function App(){
    return(
        <div>
            <h2>Welcome To React</h2>
            <MyName></MyName>
        </div>
    );
    };
```

# Using Props: #

Now That we understand how to create components, let's take a look at how to pass data into our component.

1. We want to add our data to our root component or root function. Remember data can only be passed down to components. We will add two constant variables called 
FirstName and LastName. Which will equal your first name and your last name.

```jsx
    function App(){
    const FirstName = "Jonathan";
    const LastName = "Vershum";
    
    return(
        <div>
            <h2>Welcome To React</h2>
            <MyName></MyName>
        </div>
    );
    };
```

2. Next inside of our App() component We currently have the <MyName> Component, within the <MyName> Component we will add MyFirstName={FirstName} and MyLastName={LastName}.

    <MyName MyFirstName={FirstName} MyLastName={LastName}></MyName>

>What we did here was define 2 objects that are arguments for our MyName Function 

3. Next we will access those variables inside of our MyName component using “props”. Navigate to components/MyName.js and inside of the MyName function parameter we will add props

```jsx
    function MyName(props){
    return  <h3>My Name Is:</h3>
    };
```

By adding props this will allow us to access the global props object, which is where our MyFirstName and MyLastName are currently being stored.

4. Now all we have to do is access the data within the props object. Before our closing ```</h3>``` tag is where we want our First and Last Name to appear. Here we will add ```{props.MyFirstName}``` and ```{props.MyLastName}``` which is how javascript and react is going to access the data that we passed to this function using the props object

```jsx
    function MyName(props){
    return  <h3>My Name Is: {props.MyFirstName} {props.MyLastName}</h3>
    };
        
    export default MyName;
```
